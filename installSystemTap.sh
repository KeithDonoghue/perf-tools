apt-get update 
apt-get install -y build-essential gettext elfutils libdw-dev
apt-get clean;

cd systemtap-3.1/ && \
         ./configure && \
         make all -j$(nproc) && \
         make install ;
